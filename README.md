# building

```
git clone https://gitlab.com/edneville/mod-cidr.git
cd mod-cidr
cargo build --release
```

You'll need to build a CIDR database, soemthing like this will do that:

```
    ( for i in ftp://ftp.afrinic.net/pub/stats/afrinic/delegated-afrinic-extended-latest \
          ftp://ftp.apnic.net/pub/stats/apnic/delegated-apnic-extended-latest \
          ftp://ftp.arin.net/pub/stats/arin/delegated-arin-extended-latest \
          ftp://ftp.lacnic.net/pub/stats/lacnic/delegated-lacnic-extended-latest \
          ftp://ftp.ripe.net/ripe/stats/delegated-ripencc-extended-latest; do
          wget "$i" -O - ;
    done ) | tee delegated | target/release/builder
```

This will place a ipdb.cdb file in your current directory.


Copy the module and the database file

```
rsync -avP target/release/libmod_cidr_ng.so --rsync-path 'please rsync' remote:/usr/lib/apache2/modules/libmod_cidr_ng.so
rsync -avP ipdb.cdb --rsync-path 'please rsync' remote:/usr/local/share/
```

Tell Apache where to find the DB file and how to load the module

```
echo "LoadModule cidr_ng_module        /usr/lib/apache2/modules/libmod_cidr_ng.so" | please tee /etc/apache2/mods-available/cidr-ng.load 
please a2enmod cidr-ng
```

In a vhosts file

```
CIDR_NG_FILE /usr/local/share/ipdb.cdb
```

Then reload apache

```
please systemctl reload apache2
```

With the module loaded requests will now have headers added to them with values from the CDB, along with `CIDR_match` to show the matching CIDR range:

```
< Mod-Cidr-Ng-Version: 0.2.0
< CIDR_match: 209.93.0.0/16
< CIDR_REGISTRY: arin
< CIDR_CC: US
< CIDR_TYPE: ipv4
< CIDR_FROM: 209.93.0.0
< CIDR_SIZE: 65536
< CIDR_STATE: allocated
< CIDR_DATE: 19970604
< Mod-Cidr-Ng: :-)
```

