use regex::Regex;
use ripcalc::*;
use std::convert::TryInto;
use std::io::{self, BufRead};
use std::net::{Ipv4Addr, Ipv6Addr};

fn main() {
    let stdin = io::stdin();
    let base: u64 = 2;
    let mut ranges: Vec<u32> = vec![];
    for i in 0..31 {
        ranges.push(base.pow(i).try_into().unwrap());
    }

    ranges.reverse();

    let mut cdb = cdb::CDBWriter::create("ipdb.cdb").unwrap();

    let re = Regex::new(r"^(?<rir>[a-zA-Z]+)\|(?<cc>[a-zA-Z]+)\|(?<v>ipv[46])\|(?<ip>[0-9a-f:.]+)\|(?<range>[0-9]+)\|(?<date>[0-9]+)\|(?<state>[^|]+)\|(?<sum>[a-fA-F0-9-]+)$").unwrap();
    for line in stdin.lock().lines() {
        let input = line.as_ref().unwrap();

        let caps = match re.captures(input) {
            Some(caps) => caps,
            _ => {
                continue;
            }
        };

        let cdb_line = format!("REGISTRY={rir}\0CC={cc}\0TYPE={ver}\0FROM={from}\0SIZE={size}\0STATE={state}\0DATE={date}", rir=&caps["rir"], cc=&caps["cc"], ver=&caps["v"], from=&caps["ip"], size=&caps["range"], state=&caps["state"], date=&caps["date"]);
        let cdb_line = cdb_line.as_bytes();

        let version = caps["v"].to_string();

        if version == "ipv6" {
            let range = caps["range"].parse::<u32>().unwrap();

            let ip = parse_address_mask(&caps["ip"], None, Some(range), None, false).unwrap();

            cdb.add(format!("{}/{}", ip, ip.cidr).as_bytes(), cdb_line)
                .unwrap();
        }

        if version == "ipv4" {
            let mut ip = parse_address_mask(&caps["ip"], None, None, None, false).unwrap();
            let range = caps["range"].parse::<u32>().unwrap();

            let mut remaining = range;

            'remaining: loop {
                for (cidr, i) in ranges.iter().enumerate() {
                    if &remaining > i {
                        let mut next_bin = 0;
                        let new_ip = match &ip.address {
                            Addr::V4(x) => {
                                let mut bin: u32 = 0;
                                for j in 0..32 - ip.cidr + 2 {
                                    bin |= 1 << j;
                                }
                                bin = !(bin) & u32::from(*x);
                                next_bin = bin + i;
                                Ip {
                                    address: Addr::V4(Ipv4Addr::from(bin)),
                                    cidr: cidr as u32 + 2,
                                }
                            }
                            Addr::V6(x) => {
                                let mut bin: u128 = 0;
                                for j in 0..128 - ip.cidr + 2 {
                                    bin |= 1 << j;
                                }
                                bin = !(bin) & u128::from(*x);
                                Ip {
                                    address: Addr::V6(Ipv6Addr::from(bin)),
                                    cidr: ip.cidr + 2,
                                }
                            }
                        };

                        cdb.add(format!("{}/{}", new_ip, new_ip.cidr).as_bytes(), cdb_line)
                            .unwrap();

                        ip = match &ip.address {
                            Addr::V4(_x) => Ip {
                                address: Addr::V4(Ipv4Addr::from(next_bin)),
                                cidr: cidr as u32 + 2,
                            },
                            Addr::V6(x) => {
                                let mut bin: u128 = 0;
                                for j in 0..128 - ip.cidr + 2 {
                                    bin |= 1 << j;
                                }
                                bin = !(bin) & u128::from(*x);
                                Ip {
                                    address: Addr::V6(Ipv6Addr::from(bin)),
                                    cidr: ip.cidr + 2,
                                }
                            }
                        };

                        remaining -= i;

                        continue 'remaining;
                    }
                    if i == &remaining {
                        cdb.add(format!("{}/{}", ip, cidr + 2).as_bytes(), cdb_line)
                            .unwrap();
                        remaining = 0;
                        break;
                    }
                }

                if remaining == 0 {
                    break;
                }
            }
        }
    }
    cdb.finish().unwrap();
}
