extern crate apache_rs;

use apache_rs::ffi::*;
use ripcalc::*;
use std::convert::TryInto;
use std::ffi::CStr;
use std::ffi::CString;
use std::os::raw::c_char;
use std::os::raw::c_int;
use std::os::raw::c_void;
use std::ptr::addr_of;

const VERSION: &str = env!("CARGO_PKG_VERSION");

#[derive(Debug, Clone)]
#[repr(C)]
struct CidrCfg {
    cmode: u32,
    config_rec_data: *const i8,
    config_rec_type: u32,
}

unsafe extern "C" fn our_dir_config(r: *mut request_rec) -> CidrCfg {
    // eprintln!("our_dir_config");
    let d = ap_get_module_config((*r).per_dir_config, addr_of!(cidr_ng_module)) as *const i8;

    let c: &mut CidrCfg = &mut *(d as *mut CidrCfg);

    c.clone()
}

unsafe extern "C" fn cidr_dir_conf(pool: *mut apr_pool_t, _: *mut i8) -> *mut c_void {
    // eprintln!("cidr_dir_conf");
    let c = apr_palloc(pool, std::mem::size_of::<CidrCfg>() as u64);
    libc::memset(c, 0x00, std::mem::size_of::<CidrCfg>());
    c
}

unsafe extern "C" fn cidr_server_conf(pool: *mut apr_pool_t, s: *mut server_rec) -> *mut c_void {
    // eprintln!("cidr_server_conf");
    let d = cidr_dir_conf(pool, s as *mut i8);
    let c: &mut CidrCfg = &mut *(d as *mut CidrCfg);
    c.cmode = 0;
    c.config_rec_data = std::ptr::null();
    c.config_rec_type = 0;
    d
}

unsafe extern "C" fn set_cidr_file(
    cmd: *mut cmd_parms,
    cfg: *mut c_void,
    val: *const i8,
) -> *const i8 {
    // eprintln!("set_cidr_file");

    if cmd.is_null() {
        return std::ptr::null();
    }

    let c: &mut CidrCfg = &mut *(cfg as *mut CidrCfg);
    c.config_rec_data = val;

    std::ptr::null::<i8>()
}

const FUNC: cmd_func = cmd_func {
    take1: Some(set_cidr_file),
};

const CCSS: command_rec = command_rec {
    args_how: 1,
    cmd_data: 0 as *mut c_void,
    errmsg: b"File location\x00" as *const u8 as *const c_char,
    func: FUNC,
    name: b"CIDR_NG_FILE\x00" as *const u8 as *const c_char,
    req_override: 128 | 8,
};

const CCCSS: command_rec = command_rec {
    args_how: 0,
    cmd_data: 0 as *mut c_void,
    errmsg: b"\x00" as *const u8 as *const c_char,
    func: FUNC,
    name: std::ptr::null(),
    req_override: 0,
};

#[allow(unused_unsafe)]
#[no_mangle]
pub static mut cidr_ng_module: module = module {
    version: MODULE_MAGIC_NUMBER_MAJOR as i32,
    minor_version: MODULE_MAGIC_NUMBER_MINOR as i32,
    module_index: -1,
    name: b"mod_cidr_ng\x00" as *const u8 as *const c_char,
    dynamic_load_handle: 0 as *mut c_void,
    next: 0 as *mut module,
    magic: MODULE_MAGIC_COOKIE as u64,
    rewrite_args: None,
    create_dir_config: Some(cidr_dir_conf),
    merge_dir_config: Some(merge_dir_config),
    create_server_config: Some(cidr_server_conf),
    merge_server_config: Some(merge_server_config),
    flags: 0,
    cmds: &[CCSS, CCCSS] as *const command_struct,
    register_hooks: Some(c_mod_cidr_ng_hooks),
};

unsafe extern "C" fn merge_dir_config(
    a: *mut apache_rs::ffi::apr_pool_t,
    b: *mut c_void,
    c: *mut c_void,
) -> *mut c_void {
    // eprintln!("merge_dir_config called");
    merge_config(a, b, c)
}

unsafe extern "C" fn merge_server_config(
    a: *mut apache_rs::ffi::apr_pool_t,
    b: *mut c_void,
    c: *mut c_void,
) -> *mut c_void {
    // eprintln!("merge_server_config called");
    merge_config(a, b, c)
}

unsafe extern "C" fn merge_config(
    a: *mut apache_rs::ffi::apr_pool_t,
    b: *mut c_void,
    c: *mut c_void,
) -> *mut c_void {
    // eprintln!("merge_config called");
    let d: &mut CidrCfg = &mut *(cidr_dir_conf(a, std::ptr::null_mut()) as *mut CidrCfg);
    let b: &mut CidrCfg = &mut *(b as *mut CidrCfg);
    let c: &mut CidrCfg = &mut *(c as *mut CidrCfg);

    let mut choice = 0;

    if !b.config_rec_data.is_null() {
        // let z = CStr::from_ptr(b.config_rec_data);
        // eprintln!("merge_dir_config b: {}", String::from(z.to_str().unwrap()));
        choice = 1;
    }

    if !c.config_rec_data.is_null() {
        // let z = CStr::from_ptr(c.config_rec_data);
        // eprintln!("merge_dir_config c: {}", String::from(z.to_str().unwrap()));
        choice = 2;
    }

    match choice {
        1 => {
            d.config_rec_data = apr_pstrdup(a, b.config_rec_data);
        }
        2 => {
            d.config_rec_data = apr_pstrdup(a, c.config_rec_data);
        }
        _ => {
            d.config_rec_data = apr_pstrdup(a, std::ptr::null());
        }
    }

    d as *mut _ as *mut c_void
}

extern "C" fn c_mod_cidr_ng_hooks(_: *mut apr_pool_t) {
    unsafe {
        ap_hook_header_parser(
            Some(c_mod_cidr_ng_handler),
            std::ptr::null(),
            std::ptr::null(),
            APR_HOOK_FIRST.try_into().unwrap(),
        );
    };
}

#[allow(clippy::not_unsafe_ptr_arg_deref)]
pub fn rwrite(buffer: &Vec<u8>, r: *mut request_rec) {
    unsafe { ap_rwrite(buffer.as_ptr() as *mut c_void, buffer.len() as i32, r) };
}

#[allow(clippy::not_unsafe_ptr_arg_deref)]
pub fn set_header(key: &str, value: &str, r: *mut request_rec) {
    let name = CString::new(key).expect("CString::new failed");
    let value = CString::new(value).expect("CString::new failed");
    unsafe {
        apr_table_set((*r).headers_out, name.as_ptr(), value.as_ptr());
    }
}

#[allow(clippy::not_unsafe_ptr_arg_deref)]
pub fn set_header_in(key: &str, value: &str, r: *mut request_rec) {
    let name = CString::new(key).expect("CString::new failed");
    let value = CString::new(value).expect("CString::new failed");
    unsafe {
        apr_table_set((*r).headers_in, name.as_ptr(), value.as_ptr());
    }
}

extern "C" fn c_mod_cidr_ng_handler(r: *mut request_rec) -> c_int {
    // eprintln!("-> startup");
    set_header("Mod-Cidr-Ng-Version", VERSION, r);
    let ip = unsafe {
        let ip = CStr::from_ptr((*((*r).connection)).client_ip);
        String::from(ip.to_str().unwrap())
    };

    let ip = parse_address_mask(&ip, Some(32), Some(128), None, false).unwrap();
    // let ip = parse_address_mask(&format!("103.165.94.199"), Some(32), Some(128), None, false).unwrap();

    let mut y = ip.clone();

    let cfg;
    unsafe {
        cfg = our_dir_config(r);
    }

    if cfg.config_rec_data.is_null() {
        set_header("Mod-Cidr-Ng", ":-?", r);
        // eprintln!("-> exitup");
        return OK as i32;
    }

    // eprintln!("-> {:#?}", cfg.config_rec_data);
    let c_str: &CStr = unsafe { CStr::from_ptr(cfg.config_rec_data) };

    let str_slice: &str = c_str.to_str().unwrap();
    // eprintln!("{}", str_slice);

    let path = str_slice;
    let cdb = cdb::CDB::open(path);

    if cdb.is_err() {
        // eprintln!("-> no cdb");
        set_header("Mod-Cidr-Ng", ":-[", r);
        return OK as i32;
    }

    let cdb = cdb.unwrap();

    for x in (1..=ip.cidr).rev() {
        y.cidr = x;

        let str_key = format!("{}/{}", network(&y), x);
        let key = str_key.as_bytes();
        let result = cdb.get(key);
        if result.is_none() {
            continue;
        }

        let result = result.unwrap().unwrap();
        if result.is_empty() {
            break;
        }

        let header_prefix = "CIDR";

        let header_name = format!("{}_match", header_prefix);
        set_header(&header_name, &str_key, r);
        set_header_in(&header_name, &str_key, r);

        let value = String::from_utf8(result).unwrap();
        let value: Vec<&str> = value.split('\0').collect();
        for v in value {
            let p: Vec<&str> = v.split('=').collect();
            if p.len() < 2 {
                continue;
            }
            let header_name = format!("{header_prefix}_{}", p[0]);
            let header_value = p[1];

            set_header(&header_name, header_value, r);
            set_header_in(&header_name, header_value, r);
        }

        set_header("Mod-Cidr-Ng", ":-)", r);
        return OK as i32;
    }
    set_header("Mod-Cidr-Ng", ":-/", r);

    OK as i32
}
